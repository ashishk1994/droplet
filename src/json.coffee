###
  Name : Ashish Kumar, Third Year UnderGraduate, IIIT Hyderabad
  ```Pre Project for Block Editing for Html and CSS```
  Working Since: 1st Week of March 2015
  1st Commit: Using Acorn Javascript Parser
  2nd Commit: Changed Parser to my own using line and comma split
  3rd Commit: Some minute changes required for running it on localhost
  4th Commit: Enhancing the parser for quotes and No drop in a row
  5th Commit: Add + and - feature :)
  6th Commit: [To be Commited] : Add as many tests and Check it [Testing Phase]
###


define ['droplet-helper', 'droplet-model', 'droplet-parser'], (helper, model, parser) ->
  exports = {}

  ###
    Let's Define the Node Types
    Root: Program root
    Row : Rows in csv
    Simple: Quoted Values in a row
    NotSimple: Without Quoted in a row
  ###
  NODE_TYPES = [
    'Root'
    'Row'
    'NotSimple'
    'Simple'
  ]

  FORBID_ALL = ['forbid-all']
  BLOCK_ONLY = ['block-only']

  exports.CsvScriptParser = class CsvScriptParser extends parser.Parser
    constructor: (@text, @opts = {}) ->
        super
        @lines = @text.split '\n'

    ###
      Helper Functions used in building of parser and adapter
    ###
    getBounds: (startline, startcol, endline, endcol) ->
      # Args: starline, startcol, endline, endcol
      # Return: Bounds for corresponding block
      return {
        start: {
          line: startline
          column: startcol
        }
        end: {
          line: endline
          column: endcol
        }
      }

    getColor: (Node, depth, indepth) ->
      # Args: Node
      # Return: Color for the corresponding node
      if depth%2 
        if indepth%2
          return 'value'
        else
          return 'violet'
      else
        if indepth%2
          return 'command'
        else
          return 'control'

    getSocketLevel: (Node) -> 
      # Args: Node
      # Return: Socket level for the corresponding node
      helper.ANY_DROP

    getAcceptsRule: (Node) -> 
      # Args: Node
      # Return: Socket Accepted rule for the corresponding node
      default: helper.VALUE_ONLY

    getClasses: (node) ->
      # Args: Node
      # Return: Class for the corresponding node
      if node.type is 'Simple'
        return FORBID_ALL
      else if node.type is 'NotSimple'
        return FORBID_ALL
      else
        return BLOCK_ONLY

    getNode: (loc, start, end) ->
      # Args: type, start, end, loc
      # Return: Node 
      return {
         loc: loc
         start: start
         end : end
         body: []
      }

    checkStart: (index) ->
      if index is 0
        return true
      return false

    getleftIndent: (str) ->
      # Args: String
      # Return: String with left white spaces
      return str[0...str.length - str.trimLeft().length]
      
    getrightIndent: (str) ->
      # Args: String
      # Return: String with right white spaces  
      return str[str.trimRight().length...str.length]

    jsonBlock: (Node, loc, depth, indepth) ->
      # Adds a block 
      # Args: Node
      # Return: 
      @addBlock
        bounds: loc
        depth: depth
        precedence: 0
        drop: true
        color: @getColor  Node, depth, indepth 
        classes: BLOCK_ONLY
        socketLevel: @getSocketLevel Node

    jsonSocket: (Node, loc, depth) ->
      # Addes a socket
      # Args: Node
      # Return:  
      # console.log loc
      @addSocket
        bounds: loc
        depth: depth
        precedence: 1
        classes: []
        accepts: @getAcceptsRule Node

    ###
      Assumption: only one '{' or '}' will be present in each line
    ###
    setLoc: (string) ->
      depth = 0
      loc = []
      loc.push([])
      #console.log loc
      for i,j in @lines
        for k,idx in i
          # console.log loc
          if k is '{'
            bound = @getBounds j, idx, null, null 
            if depth < loc.length
              loc[depth].push({bound:bound})
            else
              loc.push([{bound:bound}])
            #console.log loc
            depth += 1
          if k is '}'
            #console.log depth
            last = loc[depth - 1]
            loc[depth - 1][last.length - 1].bound.end.line = j
            loc[depth - 1][last.length - 1].bound.end.column = idx + 1 
            depth -= 1 
      @locations = loc

    getlocation:(key) ->
      for i,j in @lines
          loc = i.indexOf("\""+key+"\""+":")
          if loc != -1
            return [j,loc]
      return null

    markRoot: ->
        @setLoc "ashish"
        tree = JSON.parse(@text)
        console.log tree
        @mark tree, 0, 0

    ###
      Constraints:
        1. Every key and value is supposed to be double quoted
        2. Colong supposed to be after each key
        3. Good Json input string 
        4. Every key should be of different name 
    ###
    mark: (node, depth, indepth) ->  
      # Add Block
      @jsonBlock node, @locations[depth][indepth].bound, depth, indepth
      cnt = indepth
      for i,j in Object.keys(node)
        keyloc = @getlocation i
        @jsonSocket node,  @getBounds keyloc[0], keyloc[1], keyloc[0], keyloc[1] + i.length + 2, depth
        if typeof node[i] != "object"
          line = @lines[keyloc[0]].trimRight() 
          col = line.indexOf(":")
          if line[line.length-1] == ','
            @jsonSocket node, @getBounds keyloc[0], col + 1, keyloc[0], line.length - 1, depth
          else
            @jsonSocket node, @getBounds keyloc[0], col + 1, keyloc[0], line.length, depth
        else 
          if node[i] instanceof Array
            line = @lines[keyloc[0]].trimRight()
            col = line.indexOf(":")
            if line[line.length-1] == ','
              @jsonSocket node, @getBounds keyloc[0], col + 1, keyloc[0], line.length - 1, depth
            else
              @jsonSocket node, @getBounds keyloc[0], col + 1, keyloc[0], line.length, depth
          else
            @jsonSocket node, @locations[depth + 1][cnt].bound, depth
            @mark node[i], depth + 1, cnt
            cnt += 1
          
    isComment: (str) ->
      str.match(/^\s*#.*$/)?


  CsvScriptParser.parens = (leading, trailing, node, context) ->
    # "leading" is the leading text owned by the block and not its children;
    # "trailing" is similar trailing text. "node" is the Block that is being dropped,
    # and context is the Socket or Indent it is being dropped into.
    #console.log leading leading()
    #console.log trailing trailing()
    return [leading, trailing]

  CsvScriptParser.alterVal = (string, action) ->
      if action == "AddSocket"
        st = string.indexOf('{')
        end = string.indexOf('}')
        lines = string[st+1...end].split(',')
        if lines.length > 1
          lines.push("    \"Key\": \"value\"")
          blockstring = lines.join(",\n")
        else
          blockstring = "    \"Key\": \"value\""
        return '{\n'+ blockstring + '\n}'
      else if action == "DelSocket"
        st = string.indexOf('{')
        end = string.indexOf('}')
        lines = string[st+1...end].split(',')
        lines.pop()
        blockstring = lines.join(",\n")
        return '{\n'+ blockstring + '\n}'

  ### 
    No Drop Allowed in Sockets 
  ###
  CsvScriptParser.drop = (block, context, pred) ->
    if context.type is 'socket'
          return helper.FORBID
    else
        return helper.ENCOURAGE
  
  CsvScriptParser.empty = "__"
  
  return parser.wrapParser CsvScriptParser
